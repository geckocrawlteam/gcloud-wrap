const PSub=require("../PSub.js");

describe("PSub",()=>{
	it("can expect a return code",(done)=>{
		let p=PSub("ls").arg("/awfawfaweawef");
		p.run().catch(()=>{
			done();
		})
	});

	it("can run a command",async()=>{
		let res=await PSub("echo",["-n","hello"]).arg("world").run();

		expect(res).toEqual("hello world");
	});

	it("can decode json",async()=>{
		let res=await PSub("echo").arg("[1,2]").decode("json").run();
		expect(res).toEqual([1,2]);
	});

	it("fails on bad json",(done)=>{
		let p=PSub("echo").arg("[1,2] awed").decode("json");
		p.run().catch(()=>{
			done();
		})
	});
});