const GCloud=require("../GCloud.js");
const PSub=require("../PSub.js");
const fs=require("fs");

describe("GCloud",()=>{
	beforeEach(()=>{
		jasmine.DEFAULT_TIMEOUT_INTERVAL=10000;
	});
	it("can hyphenify",()=>{
		expect(GCloud.hyphenify("helloWorld")).toEqual("hello-world");
	});
	it("can have null props",async()=>{
		GCloud.test="123";
		expect(GCloud.test).toEqual("123");

		GCloud.test2=null;
		expect(GCloud.test2).toEqual(null);
	});

	// The following are not real unit tests!

	/*it("can activate and list", async ()=>{
		await GCloud.auth.activateServiceAccount({
			keyFile: "../function-deploy-account.json",
			project: "project-id-6208021581561555254"
		});

		let list=await GCloud.functions.list();
		expect(list[0].availableMemoryMb).toEqual(256);
	});*/
	/*it("can activate and list, and set project form key file", async ()=>{
		await GCloud.auth.activateServiceAccount({
			keyFileContent: fs.readFileSync("../function-deploy-account.json"),
			setProjectFromKeyFile: true
		});

		let list=await GCloud.functions.list();
		expect(list[0].availableMemoryMb).toEqual(2048);
	});*/

	it("accepts the key as an object", async ()=>{
		let o=JSON.parse(fs.readFileSync("../geckocrawl.settings.json"));
		await GCloud.auth.activateServiceAccount({
			keyFileContent: o.service_account,
			setProjectFromKeyFile: true
		});

		let list=await GCloud.functions.list();
		expect(list[0].availableMemoryMb).toEqual(2048);
	});

	it("accepts the key as an object", async ()=>{
		GCloud.dryRunNext=true;

		let proc=await GCloud.functions.deploy("crawly-gcloud-runner",{
			runtime: "nodejs8",
			triggerHttp: true,
			timeout: 540,
			memory: "2G",
			serviceAccount: "test@example.com",
			entryPoint: "runCrawlRest"
		});
		expect(proc.args.length).toEqual(15);
	});
});
