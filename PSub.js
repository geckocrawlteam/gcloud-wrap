const child_process=require("child_process");

class PSub {
	constructor(cmd, args) {
		if (!cmd)
			throw new Error("No cmd");

		this.cmd=cmd;

		if (!args)
			args=[];

		this.args=args;
		this.expectedReturnCode=0;
		this.decoding=null;
	}

	arg(arg) {
		this.args.push(arg);
		return this;
	}

	decode(decoding) {
		this.decoding=decoding;
		return this;
	}

	expect(returnCode) {
		this.expectedReturnCode=returnCode;
		return this;
	}

	run() {
		this.output="";
		this.errorOutput="";

		return new Promise((resolve,reject)=>{
			let proc=child_process.spawn(this.cmd,this.args);
			proc.stdout.on("data",(data)=>{
				this.output+=data.toString();
			});
			proc.stderr.on("data",(data)=>{
				this.errorOutput+=data.toString();
			});
			proc.on("close",(code)=>{
				if (this.expectedReturnCode!=null
						&& this.expectedReturnCode!=code) {
					reject(new Error(this.errorOutput));
					return;
				}

				let res=this.output;

				switch (this.decoding) {
					case "json":
						try {
							res=JSON.parse(res);
						}

						catch (e) {
							reject(e);
							return;
						}
						break;
				}

				resolve(res);
			});
		});
	}
}

module.exports=(cmd, args)=>{
	return new PSub(cmd,args);
}