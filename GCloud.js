const PSub=require("./PSub.js");
const tmp=require("tmp");
const fs=require("fs");

class GCloud {
	constructor() {
		this.setupProcessors();

		return new Proxy(this, {
			get: (target, prop, receiver)=>{
				if (this[prop] || this.hasOwnProperty(prop))
					return this[prop];

				return GCloud.makeDeepProxy([prop],this.call.bind(this));
			}
		});
	}

	setupProcessors() {
		this.preprocessors={
			"auth.activateServiceAccount": (o)=>{
				if (o.opts.keyFileContent) {
					let content=o.opts.keyFileContent;
					if (typeof(content!="string")
							&& !(content instanceof Buffer)
							&& !(content instanceof String))
						content=JSON.stringify(content);

					o.tmpFile=tmp.fileSync();
					fs.writeFileSync(o.tmpFile.name,content);
					o.opts.keyFile=o.tmpFile.name;
					delete o.opts.keyFileContent;
				}

				if (o.opts.setProjectFromKeyFile) {
					let key=JSON.parse(fs.readFileSync(o.tmpFile.name));
					if (!key.project_id)
						throw new Error("Key file does not specify project");

					o.opts.project=key.project_id;
					delete o.opts.setProjectFromKeyFile;
				}
			}
		};
	}

	preProcess(o) {
		if (this.preprocessors[o.cmd])
			this.preprocessors[o.cmd](o);
	}

	postProcess(o) {
		if (o.tmpFile) {
			//console.log("removing tmp file");
			o.tmpFile.removeCallback();
		}
	}

	static makeDeepProxy(path, func) {
		if (!path)
			path=[];

		return new Proxy(
			(...args)=>{
				return func(path,args);
			},{
				get: (target, prop, receiver)=>{
					return GCloud.makeDeepProxy([...path,prop],func);
				}
			}
		);
	}

	static getInstance() {
		if (!GCloud.instance)
			GCloud.instance=new GCloud();

		return GCloud.instance;
	}

	async call(path, args) {
		let o={
			path: path,
			args: [],
			opts: {}
		};

		for (let item of args) {
			if (typeof(item)=="string")
				o.args.push(item);

			else
				for (let key in item)
					o.opts[key]=item[key];
		}

		o.cmd=o.path.join(".");
		this.preProcess(o);

		let proc=PSub(__dirname+"/google-cloud-sdk/bin/gcloud")
			.arg("--format=json")
			.decode("json");

		for (let item of o.path)
			proc.arg(this.hyphenify(item));

		for (let item of o.args)
			proc.arg(item);

		for (let key in o.opts) {
			proc.arg("--"+this.hyphenify(key));
			if (typeof o.opts[key]!="boolean" || !o.opts[key])
				proc.arg(o.opts[key]);
		}

		if (this.dryRunNext) {
			this.dryRunNext=false;
			return proc;
		}
		//console.log(proc);

		let res;
		try {
			res=await proc.run();
			this.postProcess(o);
			return res;
		}

		catch (e) {
			this.postProcess(o);
			throw(e);
		}
	}

	hyphenify(s) {
		let res="";

		for (let char of s) {
			if (char==char.toUpperCase())
				res+=("-"+char.toLowerCase());

			else
				res+=char;
		}

		return res;
	}
}

module.exports=GCloud.getInstance();